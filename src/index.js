import _ from 'lodash';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
//~for rendering component do DOM:
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail'

const API_KEY = 'AIzaSyD-FSZ4vz2-iSqIa9gKEuEqT11-yVgcSpE';

//Part 1 -  Create new component. This component should produce some HTML
// make the component:
// const is ES6 syntax - declaring a variable with final value. we never going to reassign App.
// value of "this" is changed when writing without function keyword
// functional component:
class App extends Component {

    constructor(props){
        super(props);

        this.state = {
            videos: [],
            selectedVideo: null
        };

        this.videoSearch("surfboards");
    }

    videoSearch(term) {
        YTSearch({key: API_KEY, term: term}, (videos) => {
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            });
        });
    }

    render() {
        //this function can only be called once every 300 miliseconds
        const videoSearch = _.debounce((term) => { this.videoSearch(term) }, 300 );
        // html inside function? well: JSX - is subset of js that lets us write js that looks like html.
        // the webpack will differ this <div> later in the Tooling/Packing part.
        //passing props. video is a prop. any time App rerender VideoList will get the new this.state.videos.
        return (
            <div>
                <SearchBar onSearchTermChange={videoSearch}/>
                <VideoDetail video={this.state.selectedVideo}/>
                <VideoList
                    onVideoSelect={selectedVideo => this.setState({selectedVideo})}
                    videos={this.state.videos}/>
            </div>
        );
    }
}

// *** what is JSX doing for us?  what is it's purpose? we use JSX because this is what produces the actual html code from our <div>Hi!</div> html.
// why does it looks like html? because it is convinent. By "React.createElement(div,null,hi hello world)"
// Another thing! writing <div><div></div></div> will resulting ugly js code, with JSX it's clearer.

// Part 2 - Take this component's generated HTML and
// put it on the page (in the DOM)

// *** "javascript modules" - all the code we write in seperate files is seperated in from code in other project unless we explicty define it.

// whenever we write <div></div> React.createElement is calling for us. it creates an INSTANCE of the class.
// so, we can write <App> and we will actually make an INSTANCE of the variable App we created.

// second parameter: where to render to.
ReactDOM.render(<App/>, document.querySelector('.container'));