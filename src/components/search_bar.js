import React, { Component } from 'react'; //for React.createElement(..

// class base component
class SearchBar extends Component {
    //first and only function get activate automaticly when object created.
    constructor(props){
        //father constructor
        super(props);

        this.state = { term: '' };
    }
    //inherit - to tell JSX how to render our class
    render(){
        // when writing JSX and using JS variables we rap it with { }
        return (
            <div className="search-bar">
                <input onChange={event => this.onInputChange(event.target.value)}/>
            </div>);
    }

    onInputChange(term){
        this.setState({term});
        this.props.onSearchTermChange(term);
    }
}

export default SearchBar;